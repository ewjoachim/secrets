package main

import (
	"io/ioutil"
	"strings"
	"testing"

	"github.com/pelletier/go-toml"
	"github.com/stretchr/testify/assert"
)

const (
	// AWSTokenIdentifierSlug matches the AWS revocation type identifier slug
	AWSTokenIdentifierSlug = "gitleaks_rule_id_aws"
)

type GitleaksRulesConfig struct {
	Title string
	Rules []Rule
}

type Rule struct {
	Description string
	Tags        []string
}

// TestStableAWSIdentifierSlug ensures we are consistently generating
// a revocation key type matching `ScanSecurityReportSecretsWorker`
func TestStableAWSIdentifierSlug(t *testing.T) {
	revocationRules := revocationRuleTypes()
	if len(revocationRules) != 1 {
		t.Fatalf("Incorrect revocable rule types, expected %d, got %d", 1, len(revocationRules))
	}
	awsDescription := awsRuleDescription(revocationRules)

	got := Secret{
		Rule: awsDescription,
	}
	identifier := strings.ToLower(
		string(got.identifiers()[0].Type) + "_" + got.identifiers()[0].Value)

	assert.Equal(t, AWSTokenIdentifierSlug, identifier, "should be equal")
}

func awsRuleDescription(revocationRules []Rule) string {
	for _, rule := range revocationRules {
		for _, tag := range rule.Tags {
			if tag == "aws" {
				return rule.Description
			}
		}
	}
	return ""
}

func revocationRuleTypes() []Rule {
	revocationRules := []Rule{}

	for _, rule := range rulesConfig().Rules {
		for _, tag := range rule.Tags {
			if tag == "revocation_type" {
				revocationRules = append(revocationRules, rule)
			}
		}
	}
	return revocationRules
}

func rulesConfig() GitleaksRulesConfig {
	b, err := ioutil.ReadFile("./gitleaks.toml")
	if err != nil {
		panic(err)
	}

	rulesConfig := GitleaksRulesConfig{}
	if err = toml.Unmarshal(b, &rulesConfig); err != nil {
		panic(err)
	}

	return rulesConfig
}
