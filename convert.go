package main

import (
	"encoding/json"
	"io"

	log "github.com/sirupsen/logrus"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
)

func convert(reader io.Reader, prependPath string) (*report.Report, error) {
	var (
		secrets []Secret
		vulns   []report.Vulnerability
	)

	err := json.NewDecoder(reader).Decode(&secrets)
	if err != nil {
		log.Errorf("Couldn't parse the Gitleaks report: %v\n", err)
		return nil, err
	}

	for _, secret := range secrets {
		vulns = append(vulns, report.Vulnerability{
			Category:             metadata.Type,
			Scanner:              metadata.IssueScanner,
			Name:                 secret.Rule,
			Message:              secret.message(),
			Description:          secret.description(),
			CompareKey:           secret.compareKey(),
			Severity:             report.SeverityLevelCritical,
			Confidence:           report.ConfidenceLevelUnknown,
			Location:             secret.location(),
			Identifiers:          secret.identifiers(),
			RawSourceCodeExtract: truncate(secret.Offender, maxSourceCodeLen),
		})
	}

	newReport := report.NewReport()
	newReport.Version = report.Version{
		Major: 13,
		Minor: 1,
		Patch: 0,
	}
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSecretDetection
	newReport.Vulnerabilities = vulns

	return &newReport, nil
}

func truncate(str string, maxLen int) string {
	if len(str) > maxLen {
		return str[:maxLen-1]
	}
	return str
}
