package metadata_test

import (
	"reflect"
	"testing"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/secrets/v3/metadata"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "gitleaks",
		Name:    "Gitleaks",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://github.com/zricethezav/gitleaks",
	}
	got := metadata.ReportScanner

	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
