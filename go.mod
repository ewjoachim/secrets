module gitlab.com/gitlab-org/security-products/analyzers/secrets/v3

require (
	github.com/pelletier/go-toml v1.8.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.0.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.0.0
)

go 1.15
